/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.vera.springdata.config.AppConfig;
import com.vera.springdata.model.Customer;
import com.vera.springdata.logical.CustomerListProcessor;
import com.vera.springdata.logical.CustomerListProcessorImpl;
import java.util.ArrayList;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.vera.springdata.sevicies.CustomerService;

/**
 *
 * @author Wera
 */
public class AppAnnotation {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        CustomerListProcessor customerListProcessor = context.getBean(CustomerListProcessorImpl.class);
        // CustomerService customerService = context.getBean(CustomerService.class);
        ArrayList<Customer> list = (ArrayList<Customer>) customerListProcessor.getCustomerList("J");
        //  ArrayList<Customer> list = (ArrayList<Customer>)customerService.findByName("student");
        System.out.println();
        System.out.println();
        System.out.println("Customer whith prefix J");
        for (Customer x : list) {
            System.out.println(x);
        }
        System.out.println("Customer 149");
        System.out.println(customerListProcessor.getCustomerId(149));
    }

}
