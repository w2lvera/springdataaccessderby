package com.vera.springdata.repository;

import com.vera.springdata.model.Customer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository<C> extends CrudRepository<Customer, Integer>{
    List<Customer> findByName(String name);
}
