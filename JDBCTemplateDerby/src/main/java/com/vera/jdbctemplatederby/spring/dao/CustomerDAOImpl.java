package com.vera.jdbctemplatederby.spring.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.vera.jdbctemplatederby.model.Customer;
import com.vera.jdbctemplatederby.model.CustomerMapper;

@Component
public class CustomerDAOImpl implements CustomerDAO {

    JdbcTemplate jdbcTemplate;

    private final String SQL_FIND_CUSTOMER = "select * from customer where customer_id = ?";
    private final String SQL_DELETE_CUSTOMER = "delete from customer where id = ?";
    private final String SQL_UPDATE_CUSTOMER = "update customer set  name = ? where customer_id = ?";
    private final String SQL_GET_ALL = "select * from customer";
    private final String SQL_INSERT_CUSTOMER = "insert into customer(customer_id, name) values(?,?)";

    @Autowired
    public CustomerDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Customer getCustomerById(int id) {
        return jdbcTemplate.queryForObject(SQL_FIND_CUSTOMER, new Object[]{id}, new CustomerMapper());
    }

    public List<Customer> getAllCustomers() {
        return jdbcTemplate.query(SQL_GET_ALL, new CustomerMapper());
    }

    public boolean deleteCustomer(Customer customer) {
        return jdbcTemplate.update(SQL_DELETE_CUSTOMER, customer.getId()) > 0;
    }

    public boolean updateCustomer(Customer customer) {
        return jdbcTemplate.update(SQL_UPDATE_CUSTOMER, customer.getName(),
                customer.getId()) > 0;
    }

    public boolean createCustomer(Customer customer) {
        return jdbcTemplate.update(SQL_INSERT_CUSTOMER, customer.getId(), customer.getName()) > 0;
    }

}
