package com.vera.jdbctemplatederby.spring.dao;

import java.util.List;
import com.vera.jdbctemplatederby.model.Customer;
public interface CustomerDAO {
    Customer getCustomerById(int id);
    List<Customer> getAllCustomers();
    boolean deleteCustomer(Customer customer);
    boolean updateCustomer(Customer customer);
    boolean createCustomer(Customer customer);
}
