package com.vera.jdbctemplatederby.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;



public class CustomerMapper implements RowMapper<Customer> {

	public Customer mapRow(ResultSet rs, int i) throws SQLException {

		Customer person = new Customer();
                Customer customer = new Customer(
                        rs.getInt("CUSTOMER_ID"),
                        rs.getString("NAME")
                );
                
		return customer;
	}
}
