/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.jdbctemplatederby.logical;



import java.util.ArrayList;
import java.util.List;
import com.vera.jdbctemplatederby.model.Customer;
import com.vera.jdbctemplatederby.spring.dao.CustomerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wera
 */
@Component
public class CustomerListProcessorImpl implements CustomerListProcessor {

     private CustomerDAO customerDAO;
     @Override
    public List<Customer> getCustomerList(String prefix) {
        
        final List<Customer> allCustomers = customerDAO.getAllCustomers();
        final List<Customer> result = new ArrayList<Customer>();
        for (Customer customer : allCustomers) {
            if (customer.getName().startsWith(prefix)) {
                result.add(customer);
            }
        }
        return result;
    }
    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }
}
