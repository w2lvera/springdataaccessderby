package com.vera.jdbctemplatederby.logical;

import com.vera.jdbctemplatederby.spring.dao.CustomerDAO;
import com.vera.jdbctemplatederby.model.Customer;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wera
 */
public interface CustomerListProcessor {
    List<Customer> getCustomerList(String prefix);
    public void setCustomerDAO(CustomerDAO customerDAO);
}

