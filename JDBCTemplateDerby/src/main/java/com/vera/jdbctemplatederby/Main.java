package com.vera.jdbctemplatederby;

import com.vera.jdbctemplatederby.logical.CustomerListProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.vera.jdbctemplatederby.model.Customer;
import com.vera.jdbctemplatederby.spring.config.AppConfig;
import com.vera.jdbctemplatederby.spring.dao.CustomerDAO;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(AppConfig.class);

        CustomerDAO customerDAO = context.getBean(CustomerDAO.class);
        CustomerListProcessor customerListProcessor
                = context.getBean(CustomerListProcessor.class);
        ArrayList<Customer> list
                = (ArrayList) customerListProcessor.getCustomerList("J");
        System.out.println("List of person  with prefix J is:");
        String result = "";
        for (Customer x : list) {
            result += x + "\n";
        }
        System.out.println(result);
        System.out.println("List of person is:");
        for (Customer c : customerDAO.getAllCustomers()) {
            System.out.println(c);
        }
        System.out.println("\nGet person with ID 100");
        Customer customerById = customerDAO.getCustomerById(100);
        System.out.println(customerById);
//		System.out.println("\nCreating customer: ");
//		Customer customer = new Customer(1000, "Sergey");
//		System.out.println(customer);
//		customerDAO.createCustomer(customer);
//		System.out.println("\nList of customer is:");
//
//		for (Customer c : customerDAO.getAllCustomers()) {
//			System.out.println(c);
//		}

//		System.out.println("\nDeleting customer with ID 2");
//		customerDAO.deleteCustomer(customerById);
//
//		System.out.println("\nUpdate person with ID 4");
//
        Customer cc = customerDAO.getCustomerById(100);
        cc.setName("CHANGED");
        customerDAO.updateCustomer(cc);

        System.out.println("\nList of customers after change is:");
        for (Customer c : customerDAO.getAllCustomers()) {
            System.out.println(c);
        }
        //customerDAO.deleteCustomer(cc);
        context.close();
    }
}
