/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatederby.servicies;

import com.vera.hibernatederby.dao.CustomerDao;
import com.vera.hibernatederby.dao.CustomerDaoImpl;
import com.vera.hibernatederby.entity.Customer;
import java.util.List;

/**
 *
 * @author Wera
 */
public class CustomerService {
private CustomerDao customerDao ;
    public CustomerService() {
        customerDao = new CustomerDaoImpl();
    }
    public Customer findCustomer(int id) {
        return customerDao.findById(id);
    }
    public void saveCustomer(Customer customer) {
        customerDao.save(customer);
    }
    public void deleteCustomer(Customer customer) {
       customerDao.delete(customer);
    }
    public void updateCustomer(Customer customer) {
        customerDao.update(customer);
    }
    public List<Customer> findAll() {
        return customerDao.findAll();
    }
}
