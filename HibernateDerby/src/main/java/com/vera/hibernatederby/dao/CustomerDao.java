/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatederby.dao;

import com.vera.hibernatederby.entity.Customer;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Wera
 */
public interface CustomerDao {
     Customer findById(int id);
    void update(Customer customer);
    void save(Customer customer);
    void delete(Customer customer);
    List<Customer> findAll();
     
}
