/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatederby.dao;

import com.vera.hibernatederby.entity.Customer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.vera.hibernatederby.utils.HibernateSessionFactoryUtil;
import com.vera.hibernatederby.entity.Customer;


import java.util.List;
import org.hibernate.query.Query;

/**
 *
 * @author Wera
 */
public class CustomerDaoImpl implements CustomerDao {
    @Override
    public Customer findById(int id) {
       return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Customer.class, id);
    }
    @Override
    public void update(Customer customer) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    @Override
    public void save(Customer customer) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    @Override
    public void delete(Customer customer) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    @Override
    public List<Customer> findAll() {
        List<Customer> customers = (List<Customer>)  
                HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Customer").list();
        return customers;
    }
}