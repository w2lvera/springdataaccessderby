/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatederby.logical;


import com.vera.hibernatederby.dao.CustomerDao;
import com.vera.hibernatederby.entity.Customer;
import java.util.ArrayList;
import java.util.List;
import com.vera.hibernatederby.servicies.CustomerService;

/**
 *
 * @author Wera
 */
public class CustomerListProcessorImpl implements CustomerListProcessor {

     private CustomerService customerService;
     private CustomerDao customerDao;
     @Override
    public List<Customer> getCustomerList(String prefix) {
        
        final List<Customer> allCustomers = customerService.findAll();
        final List<Customer> result = new ArrayList<Customer>();
        for (Customer customer : allCustomers) {
            if (customer.getName().startsWith(prefix)) {
                result.add(customer);
            }
        }
        return result;
    }
    @Override
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
    public Customer getCustomerId(int id) {
        return customerService.findCustomer(id);
    }
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }
}
