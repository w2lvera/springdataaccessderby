package com.vera.hibernatederby.logical;

import com.vera.hibernatederby.dao.CustomerDao;
import com.vera.hibernatederby.entity.Customer;
import java.util.List;
import com.vera.hibernatederby.servicies.CustomerService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wera
 */
public interface CustomerListProcessor {
    List<Customer> getCustomerList(String prefix);
    Customer getCustomerId(int id);
    public void setCustomerService(CustomerService customerService);
}

