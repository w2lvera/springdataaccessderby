/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatederby;

import com.vera.hibernatederby.entity.Customer;
import com.vera.hibernatederby.logical.CustomerListProcessor;
import com.vera.hibernatederby.logical.CustomerListProcessorImpl;
import com.vera.hibernatederby.servicies.CustomerService;
import java.util.ArrayList;

/**
 *
 * @author Wera
 */
public class Main {

    public static void main(String[] args) {
        CustomerListProcessor processor
                = new CustomerListProcessorImpl();
        processor.setCustomerService(new CustomerService());
        ArrayList<Customer> list = (ArrayList<Customer>) processor.getCustomerList("J");
        System.out.println();
        System.out.println();
        System.out.println("Customer with prefix J");
        for (Customer x : list) {
            System.out.println(x);
        }
        System.out.println("Customer 149");
        System.out.println(processor.getCustomerId(149));
    }
}
