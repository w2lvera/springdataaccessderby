/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatespringderby.config;

/**
 *
 * @author Wera
 */
import com.vera.hibernatespringderby.entity.Customer;
import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

@Configuration
@ComponentScan(basePackages = {"com.vera.hibernatespringderby.dao",
    "com.vera.hibernatespringderby.logical", "com.vera.hibernatespringderby.servicies"
    + ""})
@PropertySource("classpath:database.properties")
public class AppConfig {

    @Autowired
    Environment environment;
    private final String URL = "url";
    private final String USER = "parol";
    private final String DRIVER = "driver";
    private final String PASSWORD = "password";
    private final String PROPERTY_SHOW_SQL = "hibernate.show_sql";
    private final String PROPERTY_DIALECT = "hibernate.dialect";

    @Bean
    DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setUrl(environment.getProperty(URL));
        driverManagerDataSource.setUsername(environment.getProperty(USER));
        driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
        driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
        return driverManagerDataSource;
    }

    @Bean
    org.springframework.orm.hibernate4.LocalSessionFactoryBean sessionFactory() {
        org.springframework.orm.hibernate4.LocalSessionFactoryBean factoryBean
                = new org.springframework.orm.hibernate4.LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource());
        //   factoryBean.setPackagesToScan("com.vera.hibernatespringberby.entity");
        factoryBean.setAnnotatedClasses(new Class<?>[]{Customer.class});
        factoryBean.setHibernateProperties(hibernateProps());
        return factoryBean;
    }

    @Bean
    Properties hibernateProps() {
        Properties properties = new Properties();
        properties.setProperty(PROPERTY_DIALECT, environment.getProperty(PROPERTY_DIALECT));
        properties.setProperty(PROPERTY_SHOW_SQL, environment.getProperty(PROPERTY_SHOW_SQL));
        return properties;
    }
}
