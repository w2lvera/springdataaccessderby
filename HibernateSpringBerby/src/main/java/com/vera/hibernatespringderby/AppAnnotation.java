package com.vera.hibernatespringderby;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.vera.hibernatespringderby.config.AppConfig;
import com.vera.hibernatespringderby.entity.Customer;
import com.vera.hibernatespringderby.logical.CustomerListProcessor;
import com.vera.hibernatespringderby.logical.CustomerListProcessorImpl;
import java.util.ArrayList;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Wera
 */
public class AppAnnotation {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        CustomerListProcessor customerListProcessor = context.getBean(CustomerListProcessorImpl.class);
        
        ArrayList<Customer> list = (ArrayList<Customer>)customerListProcessor.getCustomerList("J");
        System.out.println();
        System.out.println();
        System.out.println("Customer whith prefix J");
        for(Customer x:list)
            System.out.println(x);
        System.out.println("Customer 149");
        System.out.println(customerListProcessor.getCustomerId(149));
    }
}
