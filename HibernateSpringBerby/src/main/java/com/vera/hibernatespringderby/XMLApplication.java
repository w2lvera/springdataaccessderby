/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatespringderby;

import com.vera.hibernatespringderby.entity.Customer;
import com.vera.hibernatespringderby.logical.CustomerListProcessor;
import java.util.ArrayList;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 *
 * @author Wera
 */
public class XMLApplication {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = 
                new ClassPathXmlApplicationContext("SpringXMLConfig.xml");
        CustomerListProcessor processor =
                (CustomerListProcessor) context.getBean("customerProcessor");
        ArrayList<Customer> list = 
                (ArrayList<Customer>)processor.getCustomerList("J");
        System.out.println();
        System.out.println();
        System.out.println("Customer whith prefix J");
        for(Customer x:list)
            System.out.println(x);
        System.out.println("Customer 149");
        System.out.println(processor.getCustomerId(149));
    }
}
