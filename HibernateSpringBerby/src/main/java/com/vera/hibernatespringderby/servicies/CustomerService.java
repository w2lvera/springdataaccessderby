/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatespringderby.servicies;

//import com.vera.hibernatespringberby.dao.CustomerDao;
import com.vera.hibernatespringderby.dao.CustomerDao;
import com.vera.hibernatespringderby.dao.CustomerDaoImpl;
import com.vera.hibernatespringderby.entity.Customer;
//import com.vera.hibernatespringberby.entity.Customer;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wera
 */
@Component
public class CustomerService {

    private CustomerDao customerDao;

    @Autowired
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public CustomerService() {

    }

    public Customer findUser(int id) {
        return customerDao.findById(id);
    }

    public void saveUser(Customer customer) {
        customerDao.save(customer);
    }

    public void deleteUser(Customer customer) {
        customerDao.delete(customer);
    }

    public void updateUser(Customer customer) {
        customerDao.update(customer);
    }

    public List<Customer> findAll() {
        return customerDao.findAll();
    }
}
