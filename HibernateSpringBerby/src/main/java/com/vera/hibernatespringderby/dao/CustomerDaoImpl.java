/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.hibernatespringderby.dao;


import com.vera.hibernatespringderby.entity.Customer;


import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wera
 */
@Component
public class CustomerDaoImpl implements CustomerDao {

 private SessionFactory sessionFactory;
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 

    @Override
    public Customer findById(int id) {
       return sessionFactory.openSession().get(Customer.class, id);
    }

    @Override
    public void update(Customer user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Customer user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Customer user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = (List<Customer>)  
              sessionFactory.openSession().createQuery("From Customer").list();
        return customers;
    }

}